rmdir Bld\App /s /q

dotnet publish -c Release -o Bld/App
docker build --compress --label "kubesample" --no-cache --tag "hub.open-brocker.ru/test/app:latest" .

