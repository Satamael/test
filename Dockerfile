FROM mcr.microsoft.com/dotnet/runtime:6.0

COPY Bld/App app/
WORKDIR /app

ENTRYPOINT ["dotnet", "KubeSample.dll"]